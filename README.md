StartupPlace
==================

The startupplace official site

To install:
--------------------------------------------------------------------------------

	1) application/bootstrap.php (depending on your structure): 'base_url' => '/base/site',

	2) site/.htaccess (depending on your structure): RewriteBase /base/site

	3) modify application/config/* according to your needs
